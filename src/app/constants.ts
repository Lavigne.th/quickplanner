export enum APP_ERROR {
    USER_ALREADY_EXSISTS = "user.alreadyExists",
    WRONG_CREDENTIALS = "credentials.wrong",
    ACCOUNT_NOT_FOUND = "account.notFound"
}