import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from './service/auth.service';
import { LanguageService } from './service/language.service';
import { LocalStorageService } from './service/local-storage.service';
declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  public startLang: string;

  constructor(private languageService: LanguageService, public authService: AuthService, private router: Router) {
    this.startLang = languageService.getCurrentLanguage();
    languageService.changeLanguage(this.startLang);
  }

  ngOnInit(): void {
    $('#nav-right-menu .dropdown').dropdown({
      onChange: (value: any, text: string, $selectedItem: any) => {
        let languageStr : string = $selectedItem[0].getAttribute('data-value');
        this.languageService.changeLanguage(languageStr);
      }
    });
  }

  onToggleSideBar(): void {
    $('.ui.sidebar').sidebar({
      context: $('.bottom.segment')
    }).sidebar('toggle');
  }

  onDisconnectClicked(): void {
    this.authService.disconnect();
    this.router.navigate(['/']);
  }
}
