import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AccountInfoResponse } from '../dto/account/account-info-response';
import { BaseResponse } from '../dto/base-response';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  constructor(private http: HttpClient) { }

  getInfos(): Observable<AccountInfoResponse> {
    return this.http.get("http://localhost:8080/api/account/info");
  }

  deleteAccout(): Observable<BaseResponse> {
    return this.http.get("http://localhost:8080/api/account/delete");
  }
}
