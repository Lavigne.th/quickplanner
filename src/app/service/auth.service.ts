import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, Observable } from 'rxjs';
import { LoginRequest } from '../dto/auth/login-request';
import { LoginResponse } from '../dto/auth/login-response';
import { UserRegistrationRequest } from '../dto/auth/user-registration-request';
import { LocalStorageService } from './local-storage.service';



@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private localStorageService: LocalStorageService, private http: HttpClient) { }

  signup(payload: UserRegistrationRequest) : Observable<UserRegistrationRequest> {
    return this.http.post("http://localhost:8080/api/auth/signup", payload);
  }

  login(loginRequestPayload: LoginRequest) : Observable<LoginResponse> {
    return this.http.post<LoginResponse>("http://localhost:8080/api/auth/login", loginRequestPayload)
    .pipe(map(data => {
        this.localStorageService.set('authenticationToken', data.token);
        this.localStorageService.set('username', loginRequestPayload.pseudo);
        return data;
    }))
  }

  removeUser(): void {
    this.localStorageService.unset('authenticationToken');
    this.localStorageService.unset('username');
  }

  rememberUsername(username: string): void {
    this.localStorageService.set('username', username);
  }

  getRememberUsername(): string | null{
    return this.localStorageService.get('username');
  }

  getJwtToken(): string | null {
    return this.localStorageService.get('authenticationToken');
  }

  isAuthenticated(): boolean {
    return this.getJwtToken() !== null;
  }

  disconnect(): void {
    this.localStorageService.unset('authenticationToken');

  }
}
