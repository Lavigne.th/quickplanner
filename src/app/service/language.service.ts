import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { LocalStorageService } from './local-storage.service';

@Injectable({
  providedIn: 'root'
})
export class LanguageService {

  public readonly DEFAULT_LANGUAGE: string = 'fr-FR';

  constructor(private translateService: TranslateService, private localStorageService: LocalStorageService) {
      
  }

  changeLanguage(newLanguage: string): void {
    this.translateService.use(newLanguage);
    this.translateService.setDefaultLang(newLanguage);
    this.localStorageService.set('language', newLanguage);
  }

  getCurrentLanguage(): string {
    let storedLanguage = this.localStorageService.get('language');
    return storedLanguage === null ? this.DEFAULT_LANGUAGE : storedLanguage;
  }
}
