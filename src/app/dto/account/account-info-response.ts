import { BaseResponse } from "../base-response";

export class AccountInfoResponse extends BaseResponse {
    pseudo?: string;
    email?: string;
    nbTasks?: number;
}