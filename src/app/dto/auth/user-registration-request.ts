export class UserRegistrationRequest {
    pseudo?: string;
    password?: string;
    email?: string;
}