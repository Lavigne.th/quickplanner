import { BaseResponse } from "../base-response";

export class UserRegistrationResponse extends BaseResponse {
    pseudo?: string;
    email?: string;
}