export class BaseResponse {
    isError?: boolean;
    errorMessage?: string;
}