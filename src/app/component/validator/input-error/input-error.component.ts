import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-input-error',
  templateUrl: './input-error.component.html',
  styleUrls: ['./input-error.component.css']
})
export class InputErrorComponent implements OnInit {

  @Input("control")
  control: any;

  constructor() { }

  ngOnInit(): void {
  }

}
