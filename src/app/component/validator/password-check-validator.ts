import { Directive, Input } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, NG_VALIDATORS, ValidationErrors, Validator, ValidatorFn } from '@angular/forms';

/** A hero's name can't match the given regular expression */
export function passwordCheckValidator(passwordControl: FormControl): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
        const isSame = passwordControl.value === control.value;
        return isSame ? null : {passwordCheck: {value: false}};
    
  };
}
