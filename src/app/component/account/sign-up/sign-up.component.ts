import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { APP_ERROR } from 'src/app/constants';
import { UserRegistrationRequest } from 'src/app/dto/auth/user-registration-request';
import { UserRegistrationResponse } from 'src/app/dto/auth/user-registration-response';
import { AuthService } from 'src/app/service/auth.service';
import { showToast, TOAST_POSITION, TOAST_TYPE } from 'src/app/service/toast-service.service';
import { passwordCheckValidator } from '../../validator/password-check-validator';
declare var $: any;

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  signupForm: FormGroup;
  registerRequest: UserRegistrationRequest = new UserRegistrationRequest();
  hasTriedSubmit: boolean = false;
  isSubmiting: boolean = false;

  constructor(private authService: AuthService, private router: Router, private translate: TranslateService) { 
    var passwordControl = new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(20)]);
    this.signupForm = new FormGroup({
      username: new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(20)]),
      email: new FormControl('', Validators.email),
      password: passwordControl,
      passwordCheck: new FormControl('', [Validators.required, passwordCheckValidator(passwordControl)]),
      conditionsAccepted: new FormControl('', Validators.requiredTrue)
    });
  }

  ngOnInit(): void {
    $('.checkbox').checkbox();
  }

  onSignupSubmit(): void {
    this.hasTriedSubmit = true;
    this.registerRequest.pseudo = this.signupForm.get('username')?.value;
    this.registerRequest.password = this.signupForm.get('password')?.value;
    this.registerRequest.email = this.signupForm.get('email')?.value;
    
    if(this.signupForm.valid) {
      this.authService.signup(this.registerRequest).
    subscribe( {
        error: (e) => {
          showToast(TOAST_TYPE.ERROR, TOAST_POSITION.TOP_CENTER, this.translate.instant("wtf_error"));
        },
        next: (response: UserRegistrationResponse) => {
          if(response.isError) {
            if(response.errorMessage === APP_ERROR.USER_ALREADY_EXSISTS) {
              const errorMessage: string = this.translate.instant(APP_ERROR.USER_ALREADY_EXSISTS.toString());
              showToast(TOAST_TYPE.ERROR, TOAST_POSITION.TOP_CENTER, errorMessage);
            }
          } else {
            if(response.pseudo !== null) {
              this.authService.rememberUsername(<string>response.pseudo);
              this.router.navigate(['/login'])
              showToast(TOAST_TYPE.SUCCESS, TOAST_POSITION.TOP_CENTER, this.translate.instant("account_created"));
            } else {
              showToast(TOAST_TYPE.ERROR, TOAST_POSITION.TOP_CENTER, this.translate.instant("wtf_error"));
            }
          }
        }
      }
    );
    }
  }
}
