import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { APP_ERROR } from 'src/app/constants';
import { LoginRequest } from 'src/app/dto/auth/login-request';
import { LoginResponse } from 'src/app/dto/auth/login-response';
import { AuthService } from 'src/app/service/auth.service';
import { showToast, TOAST_POSITION, TOAST_TYPE } from 'src/app/service/toast-service.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  hasTriedSubmit: boolean = false;
  loginRequest: LoginRequest = new LoginRequest();

  errorText: string | null = null;

  constructor(private authservice: AuthService, private translate: TranslateService, private router: Router) { 
    this.loginForm = new FormGroup(
      {
        username: new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(20)]),
        password: new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(20)])
      }
    );
  }

  ngOnInit(): void {
    var username = this.authservice.getRememberUsername();
    if(username !== null) {
      this.loginForm.get('username')?.setValue(username);
    }
  }

  onLoginSubmit(): void {
    this.hasTriedSubmit = true;
    if(this.loginForm.valid) {
        this.loginRequest.pseudo = this.loginForm.get("username")?.value;
        this.loginRequest.password = this.loginForm.get("password")?.value;
        
        this.authservice.login(this.loginRequest).subscribe(
          {
            error: (e) => {
              showToast(TOAST_TYPE.ERROR, TOAST_POSITION.TOP_CENTER, this.translate.instant("wtf_error"));
            },
            next: (response: LoginResponse) => {
              this.errorText = null;
              if(!response.isError) {
                this.router.navigate(['/calendar']);
              } else {
                this.errorText = this.translate.instant(APP_ERROR.WRONG_CREDENTIALS);
              }
            }
          }
        )
    }
  }
}
