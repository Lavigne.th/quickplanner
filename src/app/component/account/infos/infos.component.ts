import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AccountInfoResponse } from 'src/app/dto/account/account-info-response';
import { BaseResponse } from 'src/app/dto/base-response';
import { AccountService } from 'src/app/service/account.service';
import { AuthService } from 'src/app/service/auth.service';
import { showToast, TOAST_POSITION, TOAST_TYPE } from 'src/app/service/toast-service.service';
declare var $: any;

@Component({
  selector: 'app-infos',
  templateUrl: './infos.component.html',
  styleUrls: ['./infos.component.css']
})
export class InfosComponent implements OnInit {

  email: string = "";
  nbTasks: number = 0;

  constructor(public authService: AuthService, private accountService: AccountService, private translate: TranslateService, private rooter: Router) { }

  ngOnInit(): void {
    this.accountService.getInfos().subscribe({
      error: (e) => {
        showToast(TOAST_TYPE.ERROR, TOAST_POSITION.TOP_CENTER, this.translate.instant("wtf_error"));
      },
      next: (response: AccountInfoResponse) => {
        if(response.isError) {
          showToast(TOAST_TYPE.ERROR, TOAST_POSITION.TOP_CENTER, this.translate.instant('wtf_error'));
        } else {
          this.email = response.email == null ? "" : response.email;
          this.nbTasks = response.nbTasks == null ? 0 : response.nbTasks;
        }
      }
    });
  }

  onDeleteAccountClicked(): void {
    $('.ui.basic.modal').modal('show');
  }

  onDeleteAccountConfirmClicked(): void {
    this.accountService.deleteAccout().subscribe({
      error: (e) => {
        showToast(TOAST_TYPE.ERROR, TOAST_POSITION.TOP_CENTER, this.translate.instant("wtf_error"));
      },
      next: (response: BaseResponse) => {
        if(response.isError) {
          showToast(TOAST_TYPE.ERROR, TOAST_POSITION.TOP_CENTER, this.translate.instant('wtf_error'));
        } else {
          this.authService.removeUser();
          this.rooter.navigate(['/']);
          showToast(TOAST_TYPE.SUCCESS, TOAST_POSITION.TOP_CENTER, this.translate.instant('account_deleted'))
        }
      }
    });
  }

}
